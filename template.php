<?php

/**
 * [EN] Force refresh of theme registry.
 *      DEVELOPMENT USE ONLY - COMMENT OUT FOR PRODUCTION
 */
 // drupal_rebuild_theme_registry();


    defined('NX_C_LINK_NAME') or define('NX_C_LINK_NAME', 'c_link');
    defined('NX_C_ITEM_NAME') or define('NX_C_ITEM_NAME', 'c_item');

    
    global $full_system_path;
           $full_system_path = check_url(url('', array('absolute' => TRUE, 'language' => language_default())));
     // -------------------------------------------------------------------------------
    global $full_theme_path;
           $full_theme_path = check_url(url(path_to_theme().'/', array('absolute' => TRUE, 'language' => language_default())));
           $full_theme_path = str_replace('?q=', '', $full_theme_path); // [EN] if not clear_url mode
     // -------------------------------------------------------------------------------
    global $nx_theme_registration_pool;
           $nx_theme_registration_pool = array();


    require_once 'forms/search-block-form-functions.inc.php';
    require_once 'forms/user-login-block-functions.tpl.php';


    function activesite_theme() {
        global $nx_theme_registration_pool;
     // ----------
        $nx_theme_registration_pool['nx-fieldset-template'] = array(
            'template' => 'nx-fieldset',
            'arguments' => array(
                'attributes' => '',
                'title' => 'title',
                'description' => 'description',
                'children' => 'children',
                'value' => 'value')
        );
     // ----------
        return $nx_theme_registration_pool;
    }


/**
 * ------------------------
 * [EN] Override or insert PHPTemplate variables into the templates
 * ------------------------
 */


    function phptemplate_preprocess(&$vars) {
        global $full_system_path;
        global $full_theme_path;
        $vars['full_system_path'] = $full_system_path;
        $vars['full_theme_path'] = $full_theme_path;
    }


    function phptemplate_preprocess_page(&$vars) {
        $tabs_pri = nx_prepare_tabs_c_items(menu_primary_local_tasks());
        $tabs_sec = nx_prepare_tabs_c_items(menu_secondary_local_tasks());
        $vars['tabs_pri'] = $tabs_pri ? '<div class="tabs tabs_pri"><div class="c_clear">&nbsp;</div><ul>' . $tabs_pri . '</ul><div class="c_clear">&nbsp;</div></div>' . "\n" : '';
        $vars['tabs_sec'] = $tabs_sec ? '<div class="tabs tabs_sec"><div class="c_clear">&nbsp;</div><ul>' . $tabs_sec . '</ul><div class="c_clear">&nbsp;</div></div>' . "\n" : '';

        $nx_body_classes = array();
            $nx_body_classes[] = $vars['is_admin'] ? 'user-admin' : 'user-not_admin';
            $nx_body_classes[] = $vars['logged_in'] ? 'logged_in-yes' : 'logged_in-no';
            $nx_body_classes[] = $vars['is_front'] ? 'page-front' : 'page-not_front';
            $nx_body_classes[] = 'layout-' . $vars['layout'];
            $nx_body_classes[] = nx_get_destination2();
            $nx_body_classes[] = 'universal_' . nx_get_destination2(TRUE);
        $vars['nx_body_classes'] = implode(' ', $nx_body_classes);
     // [EN] IE styles and version
        $vars['styles'] .= '<!--[if IE]><link type="text/css" rel="stylesheet" media="all" href="'. $full_theme_path . 'nx-style-IE.css" /><![endif]-->' . "\n";
    }


    function nx_prepare_tabs_c_items($c_item_html){
        $c_item = split('<li ', $c_item_html);
        for ($i = 1; $i < count($c_item); $i++){
            if ($i == 1) {$c_item[$i] = str_replace(NX_C_ITEM_NAME, NX_C_ITEM_NAME . ' first', $c_item[$i]);}
            if ($i == count($c_item)-1) {$c_item[$i] = str_replace(NX_C_ITEM_NAME, NX_C_ITEM_NAME . ' last', $c_item[$i]);}
        }
        return join('<li ', $c_item);
    }


   function nx_get_destination2($with_mask = FALSE){
        $destination = strip_tags((string)$_GET['q']);
        $args = explode('/', $destination);
        for ($i = 0; $i < count($args); $i++) {
            $args[$i] = str_replace(array('-',' '), '_', $args[$i]);
            if ($with_mask && is_numeric($args[$i])) {$args[$i] = 'n';}
        }
        return 'destination-'. implode('-', $args);
    }


/**
 * ------------------------
 * [EN] Generate the HTML output for a single local task link
 * ------------------------
 */


    function phptemplate_menu_local_task($link, $active = FALSE) {
        return '<li class="'. NX_C_ITEM_NAME . ($active ? ' active' : '') .'">'. $link ."</li>\n";
    }


/**
 * ------------------------
 * [EN] Return a themed set of links
 * ------------------------
 */


    function phptemplate_links($links, $attributes = array('class' => 'links'), $cover = array(
            'outer_prefix' => '',
            'inner_prefix' => '',
            'inner_suffix' => '',
            'outer_suffix' => '<span class="c_bounder">&nbsp;</span>')) {
        global $language;
        $output = '';
        if (count($links) > 0) {
            $output = '<ul'. drupal_attributes($attributes) .'>';
            $num_links = count($links);
            $i = 1;
            foreach ($links as $key => $link) {
                $class = $key;
             // [EN] Add first, last and active classes to the list of links to help out themers.
                if ($i == 1) {$class .= ' first';}
                if ($i == $num_links) {$class .= ' last';}
                if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page())) && (empty($link['language']) || $link['language']->language == $language->language)) {$class .= ' active';}
                $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';
                if (isset($link['href'])) {
                 // [EN] Pass in $link as $options, they share the same keys.
                 // -------------------------------------------------------
                    $link['title'] = $cover['inner_prefix'] . ($link['html'] ? $link['title'] : check_plain($link['title'])) . $cover['inner_suffix'];
                    $link['html'] = TRUE;
                    $output .= $cover['outer_prefix'];
                    $output .= l($link['title'], $link['href'], $link);
                    $output .= $cover['outer_suffix'];
                 // -------------------------------------------------------
                } else if (!empty($link['title'])) {
                 // [EN] Some links are actually not links, but we wrap these in <span> for adding title and class attributes
                    if (empty($link['html'])) {$link['title'] = check_plain($link['title']);}
                    $span_attributes = '';
                    if (isset($link['attributes'])) {$span_attributes = drupal_attributes($link['attributes']);}
                 // -------------------------------------------------------
                    $output .= $cover['outer_prefix'].'<span'. $span_attributes .'>'.$cover['inner_prefix'].$link['title'].$cover['inner_suffix'].'</span>'.$cover['outer_suffix'];
                 // -------------------------------------------------------
                }
                $i++;
                $output .= "</li>\n";
            }
            $output .= '</ul>';
        }
        return $output;
    }


/**
 * ------------------------
 * [EN] Return a themed set of status and/or error messages. The messages are grouped by type
 * ------------------------
 */


    function phptemplate_status_messages($display = NULL) {
        $output = '';
        foreach (drupal_get_messages($display) as $type => $messages) {
            $output .= "<div class=\"messages message-$type\">\n";
            if (count($messages) > 1) {
                $output .= " <ul>\n";
                foreach ($messages as $message) {
                    $output .= '  <li>'. $message ."</li>\n";
                }
                $output .= " </ul>\n";
            } else {
                $output .= $messages[0];
            }
            $output .= "</div>\n";
        }
        return $output;
    }


/**
 * ------------------------
 * [EN] Return a themed help message
 * ------------------------
 */


    function phptemplate_help() {
        if ($help = menu_get_active_help()) {
            if (strlen($help) > 10) {
                return '<div class="help">'. $help .'</div>';
            }
        }
    }


/**
 * ------------------------
 * [EN] Return a themed breadcrumb trail
 * ------------------------
 */


    function phptemplate_breadcrumb($breadcrumb) {
        if (!empty($breadcrumb)) {
            $last_item = &$breadcrumb[count($breadcrumb)-1];
            $last_item = str_replace('<a ', '<a class="last" ', $last_item);
            return '<div class="breadcrumb">' . implode('<span class="gt">&gt;</span>', $breadcrumb) . '</div>';
        }
    }


/**
 * ------------------------
 * [EN] Format a group of form items
 * ------------------------
 */


    function phptemplate_fieldset($element) {
        if (!empty($element['#collapsible'])) {
            if (!isset($element['#attributes']['class'])) {
                $element['#attributes']['class'] = '';
            }
            $element['#attributes']['class'] .= ' collapsible';
            if (!empty($element['#collapsed'])) {
                $element['#attributes']['class'] .= ' collapsed';
            }
        }
        return theme('nx-fieldset-template',
                     drupal_attributes($element['#attributes']),
                     ($element['#title'] ? $element['#title'] : ''),
                     (isset($element['#description']) && $element['#description'] ? $element['#description'] : ''),
                     (!empty($element['#children']) ? $element['#children'] : ''),
                     (isset($element['#value']) ? $element['#value'] : ''));
    }


/**
 * ------------------------
 * [EN] Allow themable wrapping of all comments
 * ------------------------
 */

 
    function phptemplate_comment_wrapper($content, $node) {
        if (strlen($content) == 0) {return;}
        if ($node->type == 'forum') {return '<div id="comments" class="c_all_comments">'. $content .'</div>';}
        else {return '<div id="comments" class="c_all_comments"><h2 class="c_all_comments_title">'. t('Comments') .'</h2>'. $content .'</div>';}
    }


/**
 * ------------------------
 * [EN] Return a set of blocks available for the current user
 * ------------------------
 */

    global $nx_left_column_title;


    function phptemplate_blocks($region) {
        $output = '';
        if ($list = block_list($region)) {
            $blocks_num = count($list);
            $i = 1;
            foreach ($list as $key => $block) {
             // -----------------------
                if ($block->module == 'user' && $block->delta == 1) {$block->subject = t('navigation');}
                if ($region == 'left' && $i == 1) {
                    global $nx_left_column_title;
                    $nx_left_column_title = $block->subject;
                    $block->subject = '';
                }
             // -----------------------
                if ($blocks_num == 1) {
                    $block->nx_block_extra_class = 'single';
                } else {
                    if ($i == 1) {$block->nx_block_extra_class = 'first';}
                    if ($i == $blocks_num) {$block->nx_block_extra_class = 'last';}
                }
                $block->nx_block_num = $i++;
                $output .= theme('block', $block);
            }
        }
     // [EN] Add any content assigned to this region through drupal_set_content() calls.
        $output .= drupal_get_content($region);
        return $output;
    }


/**
 * ------------------------
 * [EN] Return a themed table
 * ------------------------
 */


    function nx_set_column_number2($cell, $num){
        if (!is_array($cell)) {$cell = array('class' => '', 'data' => $cell);}
        if (!isset($cell['class'])) {$cell['class'] = '';}
        $cell['class'].= ' tc-'. $num;
        return $cell;
    }

 
    function phptemplate_table($header, $rows, $attributes = array(), $caption = NULL) {

     // [EN] Add classes to <TABLE>
        $attributes['class'] = empty($attributes['class']) ? 'table' : ($attributes['class'] .' table');

     // [EN] Add classes to HEAD <TD>/<TH>
        if (count($header)) {
            for ($i = 0; $i < count($header); $i++) {
                $header[$i] = nx_set_column_number2($header[$i], $i+1);
            }
        }

     // [EN] Add classes to BODY <TD>/<TH>
        if (count($rows)) {
            for ($j = 0; $j < count($rows); $j++) {
             // [EN] Check if we're dealing with a simple or complex row
                $tr_attributes = array();
                if (isset($rows[$j]['data'])) {
                    foreach ($rows[$j] as $key => $value) {
                        if ($key == 'data') {$cells = $value;}
                        else {$tr_attributes[$key] = $value;}
                    }
                } else {
                    $cells = $rows[$j];
                }
                if (count($cells)) {
                    for ($i = 0; $i < count($cells); $i++) {$cells[$i] = nx_set_column_number2($cells[$i], $i+1);}
                    $rows_out[] = array('data' => $cells) + $tr_attributes;
                }
            }
        }

        return '<div class="table_cover">'. theme_table($header, isset($rows_out) ? $rows_out : $rows , $attributes, $caption) .'</div>';
    }


/**
 * ------------------------
 * [EN] Generate the HTML output for a menu
 * theme_menu_tree - [EN] Generate the HTML output for a menu tree
 * theme_menu_item - [EN] Generate the HTML output for a menu item and submenu
 * theme_menu_item_link - [EN] Generate the HTML output for a single menu link
 * ------------------------
 */


    global $NX_MENU_ITEMS_POOL;
    global $NX_MENU_C_ITEM;
           $NX_MENU_ITEMS_POOL = array();
           $NX_MENU_C_ITEM = NULL;


    function phptemplate_menu_tree($tree) {
        $class = 'menu c_menu';
        global $NX_MENU_C_ITEM;
        if (isset($NX_MENU_C_ITEM['depth']) && $NX_MENU_C_ITEM['depth'] == 1){
            $class.= ($NX_MENU_C_ITEM['menu_name'] ? ' menu-' . $NX_MENU_C_ITEM['menu_name'] : '');
        }
        return '<ul class="' . $class . '">'. $tree .'</ul>';
    }


    function phptemplate_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
        $is_collapsed = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
        $class = NX_C_ITEM_NAME . ' ' . $is_collapsed;
        if (!empty($extra_class)) {$class .= ' '. $extra_class;} // [EN] .first and .last in extra_class
        if ($in_active_trail) {$class .= ' active-trail';}
     // -------------------------------------------------------------
        global $NX_MENU_ITEMS_POOL;
        $last_item = array_pop($NX_MENU_ITEMS_POOL);
        $class .= ($last_item['depth'] ? ' depth_' . $last_item['depth'] : '');
        $class .= ($last_item['depth'] ? ' ' . $is_collapsed . '-depth_' . $last_item['depth'] : '');
     // -------------------------------------------------------------
        return '<li class="'. $class .'">'. $link . $menu ."</li>\n";
    }


    function phptemplate_menu_item_link($link, $cover = array(
            'outer_prefix' => '',
            'inner_prefix' => '<span><span>',
            'inner_suffix' => '</span></span>',
            'outer_suffix' => '<span class="c_bounder">&nbsp;</span>')) {
        if (empty($link['localized_options'])) {$link['localized_options'] = array();}
     // -------------------------------------------------------------
        $options = $link['localized_options'];
        $options['attributes']['class'] = NX_C_LINK_NAME . ' ' . ($link['depth'] ? 'depth_' . $link['depth'] : '');
     // -----
        $link['title'] = $cover['inner_prefix'] . ($options['html'] ? $link['title'] : check_plain($link['title'])) . $cover['inner_suffix'];
        $options['html'] = TRUE;
     // -----
        global $NX_MENU_ITEMS_POOL, $NX_MENU_C_ITEM;
        array_push($NX_MENU_ITEMS_POOL, $link);
        $NX_MENU_C_ITEM = $link;
        return $cover['outer_prefix'] . l($link['title'], $link['href'], $options) . $cover['outer_suffix'];
    }

