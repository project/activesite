

Drupal.behaviors.collapse = function (context) {
  $('fieldset.collapsible > legend:not(.collapse-processed)', context).each(function() {

    var fieldset = $(this.parentNode),
        title = $('.title:first', fieldset),
        title_subStyle = $('.title-subStyle-2:first', title),
        title_text = title_subStyle.text(),
        fieldset_collapsible_area = $('.collapsible-area:first', fieldset);

 // [EN] Expand if there are errors inside
    if ($('input.error, textarea.error, select.error', fieldset).size() > 0) {
        fieldset.removeClass('collapsed');
    }

    title_subStyle.empty();
    title_subStyle.append($('<a href="#"></a>'));
    var title_as_link = $('a:first', title);
    title_as_link.html(title_text);

    title_as_link.mousedown(function(){return false;});
    title_as_link.click(function(){
        if (!fieldset.is('.collapsed')){fieldset.addClass('collapsed');}
        else {fieldset.removeClass('collapsed');}
        return false;
    });

    $(this).addClass('collapse-processed');

  });
};
