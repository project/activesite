
if (Drupal.jsEnabled) {
  $(document).ready(function() {

    $('.ff_clear_link').each(function() {
        var el = $(this);
        el.mousedown(function(){return!1;});
    });

 /* ---------------------------------------------------------------------------------------- */

    $('.column_ico-1').click(column_ico_1_click);

    function column_ico_1_click(){
        var page = $('.page');
        if (!page.is('.with_collapsed_panel')) {page.addClass('with_collapsed_panel');}
        else {page.removeClass('with_collapsed_panel');}
        $.cookie('with_collapsed_panel', page.is('.with_collapsed_panel')?1:0, {expires:1000, path:'/'});
        return false;
    }

    if ($.cookie('with_collapsed_panel') == 1) {
        $('.page').addClass('with_collapsed_panel');
    };

  });
}