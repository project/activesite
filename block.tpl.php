<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="c_block block <?php
    print $block_zebra;
    print ' block-'.$block_zebra;
    print ' block-'.$block->module;
    print ' block-num-'.$block->nx_block_num;
    if ($block->nx_block_extra_class) {print ' block-'.$block->nx_block_extra_class;}
?>">
<div class="c_block-subStyle">

    <?php if (!empty($block->subject)) { ?>
        <h2 class="c_block_title"><span><?php print $block->subject ?></span></h2>
    <?php } ?>

    <div class="c_block_content content">
        <?php print $block->content ?>
    </div>

</div>
</div>

<span class="c_block_bounder <?php
    if ($block->nx_block_extra_class) {
        print 'c_block_bounder-'.$block->nx_block_extra_class;
} ?>"></span>