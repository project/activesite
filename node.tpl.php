<div id="node-<?php print $node->nid; ?>" class="c_node node<?php
    if ($sticky) {print ' node-sticky';}
    if (!$status) {print ' node-unpublished';}
    print ' node-nid_'.$node->nid;
    print ' node-type_'.$node->type;
?>">
<div class="c_node-subStyle">

    <?php if ($page == 0) { ?>
        <h2 class="c_node_title">
            <a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title; ?></a>
        </h2>
    <?php } ?>

    <?php if ($submitted) { ?>
        <span class="c_node-submitted submitted">
            <?php print t('Published') .' '; ?>
            <?php print '<span class="username">' . t('!username', array('!username' => theme('username', $node))) .'</span> '; ?>
            <?php print '<span class="datetime">' . t('!datetime', array('!datetime' => format_date($node->created, 'custom', 'D, d/m/Y - H:i'))) . '</span>'; ?>
        </span>
    <?php } ?>
    
    <?php if (strlen($picture) > 50) { ?>
        <div class="c_user_picture">
            <?php print $picture ?>
        </div>
    <?php } ?>

    <div class="c_node_content content">
        <?php print $content ?>
    </div>
    
    <div class="c_clear">&nbsp;</div>

    <div class="meta">
        <?php if ($taxonomy) { ?>
            <div class="c_node_terms terms"><?php print $terms ?></div>
        <?php } ?>
    </div>

    <?php if ($links) { ?>
        <div class="c_node_links c_links"><?php print $links; ?></div>
    <?php } ?>

</div>
</div>