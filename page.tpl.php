<?php print '<?xml version="1.0"?>'; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<?php print $scripts; ?>
</head>

<?php
    if ($site_name) {$site_name = check_plain($site_name);}
    if ($site_slogan) {$site_slogan = check_plain($site_slogan);}
    $site_title = $site_name ? $site_name : t('Home');
    $if_content_with_cover = strlen($tabs_pri) || strpos($content,'<fieldset');
 ?>

<body class="<?php print $nx_body_classes; ?>">


<table class="page struct">
    <tr>
         <td class="head struct">
            <table class="columns struct">
                <tr>
                    <td class="column-1 struct">
                        <a class="logo ff_clear_link nowrap" href="<?php print check_url($front_page); ?>" title="<?php print $site_title; ?>">
                            <img src="<?php print $full_theme_path; ?>images/dotted_bounder-big.png" class="dtd_bnd-big LB" alt="" />
                            <img src="<?php print $full_theme_path; ?>logo.png" class="logo_img" alt="<?php print $site_title; ?>" />
                            <img src="<?php print $full_theme_path; ?>images/dotted_bounder-big.png" class="dtd_bnd-big RB" alt="" />
                        </a>
                        <img class="column_1-min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                    </td>
                    <td class="column-2 struct">
                        <?php print $as_header; ?>
                    </td>
                    <td class="column-3 struct">
                        <?php
                            global $user;
                            if ($user->uid) {
                                $mail = variable_get('site_mail', NULL);
                                if ($mail) {print l($mail, 'mailto:'.$mail, array('attributes' => array('class' => 'mailto_link'))) . ' ('. t('administrator') . ')';}
                                print '<img src="'.$full_theme_path.'images/dotted_bounder.png" class="dtd_bnd" alt="" />';
                                print l('<img src="'.$full_theme_path.'images/ico-logout.png" alt="" />&nbsp;&nbsp;'.t('logout'), 'logout', array('attributes' => array('class' => 'logout_link nowrap'), 'html' => TRUE));
                            }
                         ?>
                        <img src="<?php print $full_theme_path; ?>images/dotted_bounder-big.png" class="dtd_bnd-big" alt="" />
                    </td>
                </tr>
            </table>
         </td>
    </tr>

    <!-- -->

    <tr>
        <td class="before_body struct">
            <table class="columns struct">
                <tr>
                    <td class="column-1 struct" style="<?php print $left ? '' : 'display:none'; ?>">
                        <img class="column_1-min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                        <table class="column_header-struct struct">
                            <tr>
                                <td class="struct">
                                    <div class="panel_data panel_title">
                                        <div class="c_block"><div class="c_block-subStyle">
                                            <h2 class="c_block_title"><span><?php global $nx_left_column_title; print $nx_left_column_title; ?></span></h2>
                                        </div></div>
                                    </div>
                                </td>
                                <td class="column_ico-Acc struct">
                                    <a href="#" class="column_ico column_ico-1 ff_clear_link">
                                        <img src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="column-2 struct">
                        <?php
                            if (!$breadcrumb) {$breadcrumb = theme('breadcrumb', array('&nbsp;'));}
                            print $breadcrumb;
                        ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <!-- -->

    <tr>
         <td class="body struct">
            <table class="columns struct">
                <tr>
                    <td class="column-1 struct" style="<?php print $left ? '' : 'display:none'; ?>">
                        <div class="panel_data">
                            <!-- LEFT -->
                                <?php if ($left) { ?>
                                    <?php print $left; ?>
                                <?php } ?>
                            <!-- END LEFT -->
                        </div>
                        <img class="column_1-min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                    </td>
                    <td class="column-2 struct">
                        <!-- CONTENT -->
                            <?php if ($title) { print '<h1 class="c_page_title">'. $title .'</h1><div class="c_clear">&nbsp;</div>';} ?>
                            <?php print $tabs_sec; ?>
                            <?php print $tabs_pri; ?>
                            <?php if ($if_content_with_cover) { ?> <table class="content-cover"><tr><td class="content-cover-content"> <?php } ?>
                                <?php if ($show_messages && $messages) {print $messages;} ?>
                                <?php print $help; ?>
                                <?php print $content; ?>
                            <?php if ($if_content_with_cover) { ?> </td></tr></table> <?php } ?>
                        <!-- END CONTENT -->
                    </td>
                </tr>
            </table>
         </td>
    </tr>

    <!-- -->

    <tr>
        <td class="foot struct">
            <table class="columns struct">
                <tr>
                    <td class="column-1 struct">
                        &copy; 2002-2009 <a href="http://www.activemoda.ru" class="nowrap">ActiveModa</a> | <a href="http://www.activemoda.ru/tech/activesite/" class="nowrap">О программе</a>
                        <img class="column_1-min_size" src="<?php print $full_theme_path; ?>images/empty.gif" alt="" />
                    </td>
                    <td class="column-2 struct">
                        <?php print $as_footer; ?>
                        <?php if (strlen($footer_message) > 1) { ?>
                            <div class="footer_message">
                                <?php print $footer_message; ?>
                            </div>
                        <?php } ?>
                    </td>
                    <td class="column-3 struct">
                        <a href="mailto:welcome@activemoda.ru">Поддержка</a> | <span class="nowrap">Версия 3.0</span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


<div class="techno_bottom_region">
    <?php print $techno_bottom_region; ?>
</div>


<?php global $user; if ($user->uid == 0) { ?>
    <div class="preLoad">
        <div style="background:url(<?php print $full_theme_path; ?>images/fieldset-Title-collapsed-BG.png)">&nbsp;</div>
        <div style="background:url(<?php print $full_theme_path; ?>images/fieldset-Title-expanded-BG.png)">&nbsp;</div>
        <div style="background:url(<?php print $full_theme_path; ?>images/draggable-active.png)">&nbsp;</div>
    </div>
<?php } ?>

<?php print $closure; ?>
</body>
</html>